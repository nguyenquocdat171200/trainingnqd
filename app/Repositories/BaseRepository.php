<?php

namespace App\Repositories;

use App\Model\Entities\Admin;
use Illuminate\Database\Eloquent\Model;
use DB;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAll($columns = ['*'])
    {
        return $this->model->all();
    }

    public function find($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id);
    }

    public function paginate($condition = [], $limit = null, $columns = ['*'], $sort = 'asc', $sortColumn = 'id')
    {
        $conditionTmp = [
            ['del_flag', getConstant('DEL_FLAG_ON')]
        ];
        $condition = array_merge($condition, $conditionTmp);
        $limit = is_null($limit) ? config('repository.pagination.limit', 5) : $limit;
        $result = $this->model->where($condition)->orderBy($sortColumn, $sort)->paginate($limit, $columns);
        return $result;
    }

    public function whereEmail($email)
    {
        return $this->model->whereEmail($email)->first();
    }


    public function sortNameAsc($condition = [], $limit = null, $columns = ['*'])
    {
        $limit = is_null($limit) ? config('repository.pagination.limit', 5) : $limit;
        $result = $this->model->where($condition)->orderBy('name', 'asc')->paginate($limit, $columns);
        return $result;
    }

    public function create(array $input)
    {
        $input['ins_id'] = 1;
        $input['ins_datetime'] = Date('Y-m-d H:i:s');
        $input['del_flag'] = getConstant('DEL_FLAG_ON');
        return $this->model->insert($input);
    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);
        $input['upd_id'] = 1;
        $input['upd_datetime'] = Date('Y-m-d H:i:s');
        $input['del_flag'] = getConstant('DEL_FLAG_ON');
        $model->fill($input);
        $model->save();
        return $this;
    }

    public function delete($id)
    {
        $oldPhoto = $this->model->findOrFail($id);
        if ($oldPhoto->avatar == "") {
            return $this->model->where('id', $id)->update(['del_flag' => getConstant('DEL_FLAG_OFF')]);
        } else {
            if (file_exists("public/img/upload/admin/" . $oldPhoto->avatar)) {
                unlink("public/img/upload/admin/" . $oldPhoto->avatar);
                return $this->model->where('id', $id)->update(['del_flag' => getConstant('DEL_FLAG_OFF')]);
            }
        }
    }

    public function selectEmail()
    {
        return Admin::select('email')->get();
    }

    public function where($param)
    {
        return $this->model->where($param);
    }
}