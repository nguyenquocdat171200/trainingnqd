<?php

namespace App\Policies;

use App\Model\Entities\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Model\Entities\Admin $admin
     * @return mixed
     */
    public function before(Admin $admin)
    {
        if ($admin->role_type == getConstant('DEL_FLAG_ON')) {
            return true;
        }
    }

    public function viewAny(Admin $admin)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Model\Entities\Admin $admin
     * @param \App\Admin $admin
     * @return mixed
     */
    public function view(Admin $admin)
    {
        //
    }

    public function index(Admin $admin)
    {
        return $admin->role_type == 1;
    }

    public function search(Admin $admin)
    {
        return $admin->role_type == 1;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Model\Entities\Admin $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->role_type == 1;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Model\Entities\Admin $admin
     * @param \App\Admin $admin
     * @return mixed
     */
    public function update(Admin $admin)
    {
        return $admin->role_type == 1;
    }

    public function edit(Admin $admin)
    {
        return $admin->role_type == 1;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Model\Entities\Admin $admin
     * @param \App\Admin $admin
     * @return mixed
     */
    public function delete(Admin $admin)
    {

    }

    public function destroy(Admin $admin)
    {
        return $admin->role_type == 1;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Model\Entities\Admin $admin
     * @param \App\Admin $admin
     * @return mixed
     */
    public function restore(Admin $admin)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Model\Entities\Admin $admin
     * @param \App\Admin $admin
     * @return mixed
     */
    public function forceDelete(Admin $admin)
    {
        //
    }
}
