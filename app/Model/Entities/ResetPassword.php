<?php

namespace App\Model\Entities;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    protected $table = 'reset_password';
    protected $fillable = ['id', 'user_id', 'token', 'ins_id', 'upd_id', 'ins_datetime', 'upd_time', 'del_flag'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Model\Entities\User');
    }
}
