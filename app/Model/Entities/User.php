<?php

namespace App\Model\Entities;


use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = ['id', 'name', 'password', 'email', 'facebook_id', 'avatar', 'status', 'ins_id', 'ins_datetime', 'upd_datetime', 'del_flag'];
    public $timestamps = false;

    public function resetPassword()
    {
        return $this->hasMany('App\Model\Entities\Reset_password');
    }
}
