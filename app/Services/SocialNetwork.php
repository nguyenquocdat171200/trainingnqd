<?php

namespace App\Services;

use App\Model\Entities\ResetPassword;
use App\Model\Entities\User;
use App\Repositories\Admin\ResetPasswordRepository;
use App\Repositories\Admin\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use DB;
use Mail;

class SocialNetwork
{
    protected $userRepository;
    protected $resetPassword;

    public function __construct(UserRepository $userRepository, ResetPasswordRepository $resetPassword)
    {
        $this->userRepository = $userRepository;
        $this->resetPassword = $resetPassword;
    }

    public function linkFB()
    {
        $socializeUser = Socialite::with('facebook')->user();
        $facebookUserId = $socializeUser->getId(); //unique
        $name = $socializeUser->getName();
        $email = $socializeUser->getEmail();
        $avatar = "";
        $user = $this->userRepository->where(['facebook_id' => $facebookUserId])->first();
        if (!$user) {
            $user = new User;
            $user->facebook_id = $facebookUserId;
            $user->email = $email;
            $user->name = $name;
            $user->status = getConstant('STATUS_ON');
            $user->ins_id = Carbon::now();
            $user->avatar = $avatar;
            $user->save();
        }
        Auth::guard('frontend')->login($user);
    }

    public function linkGoogle()
    {
        $user = Socialite::driver('google')->user();
        $existingUser = $this->userRepository->where(['email' => $user->email])->first();
        if ($existingUser) {
            // log them in
            Auth::guard('frontend')->login($existingUser);
            return redirect()->route('home');
        } else {
            // create a new user
            $newUser = new User;
            $newUser->name = $user->getName();
            $newUser->email = $user->getEmail();
            $newUser->facebook_id = $user->getId();
            $newUser->status = getConstant('STATUS_ON');
            $newUser->ins_id = Carbon::now();
            $newUser->ins_datetime = Carbon::now();
            $newUser->del_flag = getConstant('DEL_FLAG_ON');
            $newUser->save();
            Auth::guard('frontend')->login($newUser);
            return redirect()->route('home');
        }
    }


    public function linkEmail($email)
    {
        $this->userRepository->whereEmail($email);
        $db = $this->userRepository->where(['email' => $email])->select('id')->first();
        $getId = $db->id;
        $idMh = myEncrypt($getId, 1);
        $token = md5(time() . $email);
        $dataSave['user_id'] = $getId;
        $dataSave['token'] = $token;
        $this->resetPassword->create($dataSave);
        $url = route('get.link.reset.password', ['token' => $token, 'id' => $idMh]);
        $data = [
            'view' => 'sendemail.check_email',
            'route' => $url,
        ];
        Mail::to($email)->send(new \App\Mail\SendMail($data));
    }

    public function updatePass($password, $token)
    {
        $db = $this->resetPassword->where(['token' => $token])->select('user_id')->first();
        $user_id = $db->user_id;
        ResetPassword::where('user_id', '=', $user_id)->where('token', '=', $token)->update([
            'del_flag' => getConstant('DEL_FLAG_OFF'),
            'upd_datetime' => Carbon::now(),
        ]);
        $dataSave['password'] = Hash::make($password);
        $this->userRepository->update($dataSave, $user_id);
    }
}