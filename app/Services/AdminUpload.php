<?php

namespace App\Services;

use App\Model\Entities\User;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;
use Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Model\Upload;


class AdminUpload extends Model
{
    protected $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    //update row
    public function ModelUpdate($id, $dataSave)
    {
        $dataSave['status'] = Request::get('status') == true ? $dataSave['status'] = 0 : 1;
        unset($dataSave['avatar']);
        $this->adminRepository->update($dataSave, $id);
        if (Request::get('password') == true) {
            $dataSave['password'] = Hash::make(Request::get("password"));
            $this->adminRepository->update($dataSave, $id);
        }
        //if user option image then upload image
        $file = Request::hasFile("avatar");
        if ($file) {
            $oldPhoto = User::where('id', $id)->select('avatar')->first();
            if ($oldPhoto->avatar == "") {
                $avatar = rand(1, 100000) . "____" . Request::file("avatar")->getClientOriginalName();
                Request::file("avatar")->move("public/img/upload/admin/", $avatar);
                $dataSave['avatar'] = $avatar;
                $this->adminRepository->update($dataSave, $id);
            } else {
                $avatar = rand(1, 100000) . "____" . Request::file("avatar")->getClientOriginalName();
                Request::file("avatar")->move("public/img/upload/admin/", $avatar);
                if (file_exists('public/img/upload/admin/' . $oldPhoto->avatar)) {
                    unlink("public/img/upload/admin/" . $oldPhoto->avatar);
                }
                $dataSave['avatar'] = $avatar;
                $this->adminRepository->update($dataSave, $id);
            }
        }
    }

    public function modelCreate($dataSave)
    {
        $avatar = "";
        $dataRequest = Session::get('SESSION_STORE');
        if (isset($dataRequest)) {
            if (Request::hasFile("avatar")) {
                $avatar = Request::file("avatar")->getClientOriginalName();
                Request::file("avatar")->move("public/img/upload/admin/", $avatar);
            } else if (!empty($dataRequest['avatar'])) {
                $temp = $dataRequest['avatar'];
                $getName = explode('/', $temp);
                $imgOld = "public/img/upload/temp/" . $getName[5];
                $imgNew = "public/img/upload/admin/" . $getName[5];
                if (file_exists($imgOld)) {
                    $avatar = $getName[5];
                    rename($imgOld, $imgNew);
                }
            }
        } else {
            if (Request::hasFile("avatar")) {
                $avatar = rand(1, 100000) . "____" . Request::file("avatar")->getClientOriginalName();
                Request::file("avatar")->move("public/img/upload/admin/", $avatar);
            }
        }
        $dataSave['avatar'] = $avatar;
        $dataSave['password'] = Hash::make(Request::get("password"));
        $dataSave['status'] = Request::get('status') == true ? $dataSave['status'] = 0 : 1;
        $this->adminRepository->create($dataSave);
    }

    public function uploadImg()
    {
        $url = '';
        if (Request::hasFile("avatar")) {
            //get name
            $avatar = rand(1, 100000) . "____" . Request::file("avatar")->getClientOriginalName();
            //upload image
            Request::file("avatar")->move("public/img/upload/temp/", $avatar);
            $url = '/public/img/upload/temp/' . $avatar;
        }
        return $url;
    }

}