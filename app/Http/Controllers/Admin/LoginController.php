<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoneAdminLoginRequest;
class LoginController extends Controller
{
    //form login
    public function formLoginUser()
    {
        return view('admin.login');
    }

    //login admin
    public function loginUser(StoneAdminLoginRequest $request)
    {
        $data = $request->only('email', 'password');
        if (Auth::guard('admin')->attempt($data)) {
            $user = backendGetCurrentUser();
            switch ($user['role_type']) {
                case getConstant('ROLE_TYPE_ZERO') :
                    return redirect()->route('indexBackend');
                case getConstant('ROLE_TYPE_ONE') :
                    return redirect()->route('index');
                case getConstant('ROLE_TYPE_TWO')  :
                    return redirect()->route('index');
            }
        } else {
            return back()->with('error', trans('message.checkLogin'));
        }
    }

    //logout
    public function logOut(Request $request)
    {
        backendGuard()->logout();
        return view('admin.login');
    }
}
