<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoneAdminRequest;
use App\Model\Entities\Admin;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    const SESSION_STORE = 'SESSION_STORE';
    protected $adminRepository;
    protected $adminUpload;
    protected $_adminEntity;

    public function __construct(AdminRepository $adminRepository, \App\Services\AdminUpload $adminUpload)
    {
        $this->adminRepository = $adminRepository;
        $this->adminUpload = $adminUpload;
        $this->setAdminEntity(backendGetCurrentUser());
    }

    public function getAdminEntity()
    {
        return $this->_adminEntity;
    }

    public function setAdminEntity($adminEntity)
    {
        $this->_adminEntity = $adminEntity;
    }

    public function callAction($method, $parameters)
    {
        $this->_adminEntity = Auth::guard('admin')->user();
        return call_user_func_array([$this, $method], $parameters);
    }

    public function search(Request $request)
    {
        if ($this->_adminEntity->can('search', Admin::class)) {
            $keywordOld = $request->keyword;
            $keyword = preg_replace('!%!', ' ', $keywordOld);
            $condition = [
                ['name', 'like', '%' . $keyword . '%'],
            ];
            $data = $this->adminRepository->paginate($condition, getConstant('PAGINATE'));
            return view('admin.pages.admin.all', compact('data'));
        }
    }


    public function index(Request $request)
    {
        if (isset($_GET['sort']) && isset($_GET['column'])) {
            $sort = $_GET['sort'];
            $column = $_GET['column'];
            $admin = backendGetCurrentUser();
            if ($admin->can('index', Admin::class)) {
                if ($request->session()->has('SESSION_STORE')) {
                    session()->forget(self::SESSION_STORE);
                }
                return view('admin.pages.admin.all', (['data' => $this->adminRepository->paginate([], getConstant('PAGINATE'), ['*'], $sort, $column)]));
            }
        } else {
            $admin = backendGetCurrentUser();
            if ($admin->can('index', Admin::class)) {
                if ($request->session()->has('SESSION_STORE')) {
                    session()->forget(self::SESSION_STORE);
                }
                return view('admin.pages.admin.all', (['data' => $this->adminRepository->paginate([], getConstant('PAGINATE'))]));
            }
        }
    }

    public function create(Request $request)
    {
        if ($this->_adminEntity->can('create', Admin::class)) {
            return view('admin.pages.admin.add_edit');
        }
    }

    public function store(StoneAdminRequest $request)
    {
        $dataSave = $request->all();
        unset($dataSave['_token']);
        $this->adminUpload->modelCreate($dataSave);
        return redirect()->route('index')->with('success', trans('message.add'));
    }

    public function edit($id)
    {
        if ($this->_adminEntity->can('edit', Admin::class)) {
            $edit = $this->adminRepository->find($id);
            if (empty($edit)) {
                return view('404');
            }
            $viewData = [
                'edit' => $edit
            ];
            return view('admin.pages.admin.add_edit', $viewData);
        } else {
            echo "not permision access";
        }
    }

    public function update(StoneAdminRequest $request, $id)
    {
        $avatar = $request->file('avatar');
        if ($avatar == " ") {
            $dataSave = $request->all();
            $this->adminUpload->ModelUpdate($id, $dataSave);
        } else {
            $dataSave = $request->all();
            unset($dataSave['password']);
            unset($dataSave['_token']);
            $this->adminUpload->ModelUpdate($id, $dataSave);
        }
        return redirect()->route('index')->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        if ($this->_adminEntity->can('edit', Admin::class)) {
            $this->adminRepository->delete($id);
            return redirect()->route('index')->with('success', trans('message.delete'));
        }
    }
}
