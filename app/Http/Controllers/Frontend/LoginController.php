<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function loginUser()
    {

    }

    public function home()
    {
        return view('welcome');
    }

    public function formLoginUser()
    {
        return view('user.login');
    }

    public function logout()
    {
        Auth::guard('frontend')->logout();
        return view('user.login');
    }
}
