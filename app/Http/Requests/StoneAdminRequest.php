<?php

namespace App\Http\Requests;

use App\Services\AdminUpload;
use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoneAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $adminUpload;
    const SESSION_STORE = 'SESSION_STORE';

    public function __construct(AdminUpload $adminUpload)
    {
        $this->adminUpload = $adminUpload;
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        if (!isset($id)) {
            $email = 'required|unique:users,email';
            $password = 'required|min:2|alpha_num|max:555';
            $avatar = 'mimes:jpg,jpeg,png,bmp,tiff|max:4096';
        } else {
            $email = 'required|unique:users,email,' . $id;
            $password = '';
            $avatar = 'mimes:jpg,jpeg,png,bmp,tiff|max:4096';
        }
        return [
            'name' => 'required|min:2|max:255',
            'email' => $email,
            'password' => $password,
            'avatar' => $avatar,
        ];
    }

    public function withValidator($validator)
    {
        //validate error then in
        if ($validator->fails()) {
            $dataRequest = request()->all();
            session()->put(self::SESSION_STORE, $dataRequest);
            $img = $this->adminUpload->uploadImg();
            if (!empty($img)) { //ảnh tồn tại
                $dataRequest['avatar'] = $img;
                session()->put(self::SESSION_STORE, $dataRequest);
            }
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    public function after($id, Request $request)
    {

    }

}
