<?php

namespace App\Http\Requests;

use Route;
use Illuminate\Foundation\Http\FormRequest;

class StoneFrontendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $create = Route::current();
        $reset  = Route::current();
        if (!empty($create->uri == 'store')) {
            $name = 'required|min:2|max:255';
            $email = 'required|unique:users,email';
            $password = 'required|min:2|alpha_num|max:555';
            $passwordConfirm = '';
        } elseif (!empty($reset->uri == 'updatePassword')) {
            $name = '';
            $email = '';
            $password = 'required|min:2|alpha_num|max:555';
            $passwordConfirm = 'required|min:2|alpha_num|max:555|same:password';
        }
        return [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            're_password' => $passwordConfirm,
        ];
    }
}
