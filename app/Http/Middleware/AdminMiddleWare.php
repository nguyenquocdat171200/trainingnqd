<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Admin\UserController;
use Closure;
use Auth;
use Validator;
use App\Model\Entities\Admin;

class AdminMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (backendGuard()->check()) {
            if (backendGetCurrentUser()->role_type == getConstant('ROLE_TYPE_ZERO')) {
                return $next($request);
            } else if (backendGetCurrentUser()->role_type == getConstant('ROLE_TYPE_ONE')){
                return $next($request);
            } else if (backendGetCurrentUser()->role_type == getConstant('ROLE_TYPE_TWO')){
                return $next($request);
            }
        }
        return redirect()->route('login');
    }
}
