<?php

use Illuminate\Support\Facades\Route;

Route::match(['get', 'post'], 'loginUser', [
    'as' => 'loginFacebook',
    'uses' => Request::isMethod('post') ? 'LoginController@loginUser' : 'LoginController@formLoginUser'
]);
Route::group(['middleware' => 'FrontEndMiddleware'], function () {
    Route::get('/auth/facebook', 'UserController@redirectToProvider');
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('/auth/facebook/callback', 'UserController@handleProviderCallback');
    Route::get('create', 'UserController@create');
    Route::post('store', 'UserController@store')->name('create');
    Route::get('forgot_password', 'UserController@forgot_password');
    Route::post('password', 'UserController@password');
    //update password
    Route::post('updatePassword', 'UserController@updatePassword')->name('get.link.reset.password');
    //link url mail
    Route::get('reset_password}', 'UserController@resetPassword')->name('get.link.reset.password');
    Route::get('auth/google', 'UserController@redirectToProviderGoogle');
    Route::any('auth/google/callback', 'UserController@handleProviderCallbackGoogle');
    Route::get('/', 'LoginController@home')->name('home');
});
