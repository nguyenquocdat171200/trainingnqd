<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login
Route::match(['get', 'post'], 'login', [
    'as' => 'login',
    'uses' => Request::isMethod('post') ? 'LoginController@loginUser' : 'LoginController@formLoginUser'
]);

Route::group(['middleware' => 'AdminMiddleWare'], function () {
    Route::get('/', function () {
        return view('admin.pages.index');
    })->name('indexBackend');
    Route::get('logOut', 'LoginController@logOut')->name('logOut');
    Route::get('search', 'UserController@search')->name('search');
    Route::get("users/all", "UserController@index")->name('index');
    Route::get("users/create", "UserController@create")->name('create');
    Route::post("users/create", "UserController@store")->name('backend.post.users.create');
    Route::get("users/edit/{id}", "UserController@edit")->name('edit');
    Route::post("users/edit/{id}", "UserController@update");
    Route::get("users/delete/{id}", "UserController@destroy");
});








