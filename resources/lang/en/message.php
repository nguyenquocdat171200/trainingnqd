<?php
return [
    'delete' => 'You have successfully deleted ',
    'add' => 'You have successfully add ',
    'edit' => 'You have successfully edit ',
    'error' => 'Error',
    'update' => 'You have successfully update ',
    'time' => 'Time to change password',
    'checkLogin' => 'UserName or Password not icorrect',
    'createForm' => 'Create',
    'updateForm' => 'Update',
    'hide' => 'Hide',
    'show' => 'Show',

];