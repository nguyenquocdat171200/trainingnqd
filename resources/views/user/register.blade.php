<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Register</title>

    <!-- Custom fonts for this template-->
    <link href="{{asset('public/login/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('public/login/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                        </div>
                        {{--notification--}}
                        @if ($errors->any())
                            <div style="margin-top: 20px;" class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            {!! Form::open(['method' => 'POST','url'=>'store']) !!}
                            @csrf
                            <div class="form-group row">
                                {!! Form::text('name', old('name') ,['id'=>'exampleFirstName','class'=>'form-control form-control-user','placeholder'=>'First Name'])  !!}
                            </div>
                            <div class="form-group">
                                {!! Form::email('email', old('email') ,['id'=>'exampleInputEmail','class'=>'form-control form-control-user','placeholder'=>'Email Address'])  !!}
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    {!! Form::password('password' ,['id'=>'exampleInputPassword','class'=>'form-control form-control-user','placeholder'=>'Password'])  !!}
                                </div>
                            </div>
                            {!! Form::checkbox('status') !!}Status
                            {!!  Form::submit( 'Register', ['class' => 'btn btn-primary btn-user btn-block']) !!}

                        <hr>
                            <a href="" class="btn btn-google btn-user btn-block">
                                <i class="fab fa-google fa-fw"></i> Register with Google
                            </a>
                            <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                            </a>
                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="{{url('forgot_password')}}">Forgot Password?</a>
                        </div>
                        <div class="text-center">
                            <a class="small" href="{{url('loginUser')}}">Already have an account? Login!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap core JavaScript-->
<script src="{{asset('public/login/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/login/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('public/login/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('public/login/js/sb-admin-2.min.js')}}"></script>

</body>

</html>
