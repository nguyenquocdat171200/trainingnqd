@extends('admin.master_view')
@section('main')
    @php
        $dataRequest = Session::get('SESSION_STORE');
    @endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User
                <small>Thêm mới</small>
            </h1>
            {{--notification--}}
            @if ($errors->any())
                <div style="margin-top: 20px;" class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (Session::has('error'))
                <div class="alert alert-info">{{ Session::get('error') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            {{--end notification--}}
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Tin tức</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements disabled -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">General information</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                        {!! Form::open(['method' => 'POST','enctype' => 'multipart/form-data', 'route' => empty($edit->id) ? ['backend.post.users.create'] : ['edit', $edit->id]]) !!}
                        @csrf
                        <!-- text input -->
                            <div class="form-group">
                                @if (!isset($edit->name))
                                    @php $valueName = old('name'); @endphp
                                @elseif (isset($edit->name))
                                    @php $valueName = $edit->name; @endphp
                                @endif
                                {!!   Form::label('name', 'Name' , ['style'=>'color:red;font-size:18px' ])  !!}
                                {!! Form::text('name',$valueName ,['class'=>'form-control','placeholder'=>'Enter ...']) !!}
                            </div>
                            <div class="form-group">
                                @if (!isset($edit->email))
                                    @php $valueEmail = old('email'); @endphp
                                @elseif (isset($edit->email))
                                    @php $valueEmail = $edit->email; @endphp
                                @endif
                                {!! Form::label('email','Email',['style'=>'color:red;font-size:18px']) !!}
                                {!! Form::email('email',  $valueEmail  , ['class' => 'form-control','placeholder'=>'Enter ...']) !!}
                            </div>
                            <div class="form-group">
                                @if (isset($edit->email))
                                    @php $valuePlaceholder = "Không đổi password thì không nhập thông tin vào ô textbox này" ; @endphp
                                @else
                                    @php $valuePlaceholder =  "Enter ..."; @endphp
                                    @php $valuePassword = value(old('password')); @endphp
                                @endif
                                {!! Form::label('password','Password',['style'=>'color:red;font-size:18px']) !!}
                                {!! Form::password('password',['class'=>'form-control', 'placeholder' => $valuePlaceholder]) !!}
                            </div>

                            <div class="input-group">
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        {!!   Form::file('avatar', ['style' => 'width: 100px;background: #ff0000','class'=>'btn btn-primary','id'=>'photo','accept'=>'image/x-png,image/gif,image/jpeg']) !!}
                                    </div>
                                </div>
                                @if(isset($edit->avatar) && file_exists('public/img/upload/admin/'.$edit->avatar))
                                    <img id="img-photo" name="avatar"
                                         src="{{asset('public/img/upload/admin')}}/{{$edit->avatar}}"
                                         style="max-width: 200px;">

{{--                                    <input type="hidden" name="hidden_image" value="">--}}
                                    {!! Form::hidden('hidden_image') !!}
                                @else
                                    <img id="img-photo"
                                         src="{{ !empty($dataRequest['avatar']) ? asset($dataRequest['avatar']) : asset('public/img/upload/null.png')}}"
                                         style="max-width: 200px;">
                                @endif
                            </div>
                            <img id="holder" style="margin-top:15px;max-height:100px;">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            {!!  Form::checkbox('status', 'Status', isset($edit->status) && $edit->status == 0 ? true : false) !!}
                                            Status
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                {!!  Form::reset('Làm lại', ['class' => 'btn btn-default']) !!}
                                {!!  Form::submit(isset($edit->id) ? trans('message.updateForm') : trans('message.createForm'), ['class' => 'btn btn-info pull-right']) !!}
                            </div>
                            {!! Form::close() !!}

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <a style="font-size: 25px;" href="{{ URL::previous() }}"><i
                            class="fa fa-backward btn btn-info pull-left"></i></a>
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop()





