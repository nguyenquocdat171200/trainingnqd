<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('public/admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                @php
                    $user = Auth::guard('admin')->user('name');

                @endphp
                <p>{{ $user['name'] }} </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <!-- danh mục sản phẩm 1-->

            @if(Auth::guard('admin')->user()->role_type == 0  || Auth::guard('admin')->user()->role_type == 1)
                <li class=" treeview">
                    <a href="#">
                        <i class="fa fa-industry"></i> <span>User</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <span class="label label-primary pull-right"></span>
            </span>
                    </a>
                    <ul class="treeview-menu">

                        <li class=""><a href="{{ url('backend/users/create') }}"><i class="fa fa-circle-o"></i>Thêm mới</a>
                        </li>


                        <li class=""><a href="{{ url('backend/users/all') }}"><i class="fa fa-circle-o"></i>Danh
                                sách<span
                                        class="label label-primary pull-right"></span></a></li>


                    </ul>
                </li>
            @endif

            @if(Auth::guard('admin')->user()->role_type == 0 || Auth::guard('admin')->user()->role_type == 2)
                <li class=" treeview">
                    <a href="#">
                        <i class="fa fa-industry"></i> <span>Cate</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <span class="label label-primary pull-right"></span>
            </span>
                        <!--                    --><?php
                        //                        dd(Auth::guard('admin')->user()->role_type == 0);
                        //                    ?>
                    </a>
                    <ul class="treeview-menu">

                        <li class=""><a href="{{ url('backend/users/create') }}"><i class="fa fa-circle-o"></i>Thêm mới</a>
                        </li>


                        <li class=""><a href="{{ url('backend/users/all') }}"><i class="fa fa-circle-o"></i>Danh
                                sách<span
                                        class="label label-primary pull-right"></span></a></li>


                    </ul>
                </li>
        @endif
        <!--end danh mục sản phẩm 1-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>